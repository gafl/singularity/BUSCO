#!/bin/bash

# Bacteria
wget http://busco.ezlab.org/v2/datasets/bacteria_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/proteobacteria_odb9.tar.gz

# Eukaryota
wget http://busco.ezlab.org/v2/datasets/eukaryota_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/fungi_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/microsporidia_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/dikarya_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/ascomycota_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/pezizomycotina_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/eurotiomycetes_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/sordariomyceta_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/saccharomyceta_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/saccharomycetales_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/basidiomycota_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/metazoa_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/nematoda_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/arthropoda_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/insecta_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/endopterygota_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/hymenoptera_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/diptera_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/vertebrata_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/actinopterygii_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/tetrapoda_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/aves_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/mammalia_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/euarchontoglires_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/laurasiatheria_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/embryophyta_odb9.tar.gz
wget http://busco.ezlab.org/v2/datasets/protists_ensembl.tar.gz
wget http://busco.ezlab.org/v2/datasets/alveolata_stramenophiles_ensembl.tar.gz
wget https://busco.ezlab.org/datasets/prerelease/solanaceae_odb10.tar.gz
wget https://busco.ezlab.org/datasets/prerelease/eudicotyledons_odb10.tar.gz
wget https://busco.ezlab.org/datasets/prerelease/liliopsida_odb10.tar.gz
wget https://busco.ezlab.org/datasets/prerelease/embryophyta_odb10.tar.gz
wget https://busco.ezlab.org/datasets/prerelease/chlorophyta_odb10.tar.gz
wget https://busco.ezlab.org/datasets/prerelease/viridiplantae_odb10.tar.gz


for i in $(ls *.tar.gz)
do
tar -xzf $i 
done

rm -f *.tar.gz

exit 0


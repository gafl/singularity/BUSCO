#!/bin/bash

nb=$#
ar=$1

if [ "$nb" -eq "0" ]
then
	echo "NO arg"
	exit 0
fi


arc=$(echo $ar|sed 's/\-//g'|sed 's/\s//g')

if [ "$arc" == "" ]
then
        echo "arg: NO"
        exit 0
fi


if [ "$arc" == "h" ]
then
	echo "arg:h"
	exit 0
fi

if [ "$arc" == "help" ]
then
        echo "arg:help"
        exit 0
fi

if [ "$arc" == "v" ]
then
        echo "arg:v"
        exit 0
fi

if [ "$arc" == "version" ]
then
        echo "arg:version"
        exit 0
fi

#export IFS='-'

cnt=1

# Printing the data available in $*
#echo "Values of \"\$*\":"
#for arg in "$*"
#do
#  echo "Arg #$cnt= $arg"
#  let "cnt+=1"
#done

cnt=1

# Printing the data available in $@
echo "Values of \"\$@\":"

echo $@
echo $#

for arg in "$@"
do
  echo "Arg #$cnt= $arg"
  let "cnt+=1"
done




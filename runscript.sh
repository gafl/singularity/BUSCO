#!/bin/sh 

echo "**************************************************************************"
echo "-------------------------------------"
echo "Container BUSCO v 3.0.2"
echo "-------------------------------------"
echo ""
echo "config: "
echo "AUGUSTUS_CONFIG_PATH=/busco/augustus/config/"
echo "BUSCO_CONFIG_FILE=/busco/config/config.ini"
echo ""
echo "lineage list in /busco/data/:"
echo "actinopterygii_odb9"
echo "alveolata_stramenophiles_ensembl"
echo "arthropoda_odb9"
echo "ascomycota_odb9"
echo "aves_odb9"
echo "bacteria_odb9"
echo "basidiomycota_odb9"
echo "chlorophyta_odb10"
echo "dikarya_odb9"
echo "diptera_odb9"
echo "embryophyta_odb10"
echo "embryophyta_odb9"
echo "endopterygota_odb9"
echo "euarchontoglires_odb9"
echo "eudicotyledons_odb10"
echo "eukaryota_odb9"
echo "eurotiomycetes_odb9"
echo "fungi_odb9"
echo "getall.sh"
echo "hymenoptera_odb9"
echo "insecta_odb9"
echo "laurasiatheria_odb9"
echo "liliopsida_odb10"
echo "mammalia_odb9"
echo "metazoa_odb9"
echo "microsporidia_odb9"
echo "nematoda_odb9"
echo "pezizomycotina_odb9"
echo "proteobacteria_odb9"
echo "protists_ensembl"
echo "saccharomyceta_odb9"
echo "saccharomycetales_odb9"
echo "solanaceae_odb10"
echo "sordariomyceta_odb9"
echo "tetrapoda_odb9"
echo "vertebrata_odb9"
echo "viridiplantae_odb10"
echo ""
echo "**************************************************************************"
echo "Usage, run the image: busco"
echo "**************************************************************************"
#echo "Usage default: by default the container run run_busco"
mkdir -p ${PWD}/augconf

nb=$#
ar=$1
noa=0
arc=""

if [ "$nb" -eq "0" ]
then
        noa=1
fi

arc=$(echo $ar|sed 's/\-//g'|sed 's/\s//g')

if [ "$arc" = "" ]
then
        noa=1
fi
if [ "$arc" = "h" ]
then
        noa=1
fi
if [ "$arc" = "help" ]
then
        noa=1
fi
if [ "$arc" = "v" ]
then
        noa=2
fi
if [ "$arc" = "version" ]
then
        noa=2
fi

if [ "$noa" -eq "1" ]
then
	exec /opt/miniconda/bin/run_busco --help
	exit 0
fi
if [ "$noa" -eq "2" ]
then
        exec /opt/miniconda/bin/run_busco --version
        exit 0
fi

cp -fr /busco/augustus/config/* ${PWD}/augconf
export AUGUSTUS_CONFIG_PATH="${PWD}/augconf"
exec /opt/miniconda/bin/run_busco "$@"
rm -fr ${PWD}/augconf

